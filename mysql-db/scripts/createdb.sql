CREATE DATABASE example01;

USE example01;

CREATE USER 'johnquser'@'localhost' IDENTIFIED BY 'password';

CREATE TABLE pet (
  name VARCHAR(20), 
  owner VARCHAR(20),
  species VARCHAR(20), 
  sex CHAR(1), 
  birth DATE, 
  death DATE);

INSERT INTO pet VALUES 
    ('Puffball','Diane','hamster','f','1999-03-30',NULL),
    ('Motor','Avery','cat','f','2007-01-23',NULL),
    ('Harvey','Marlo','dog','m','2016-04-03',NULL),
    ('Buddy','Ann','dog','m','2000-12-25',NULL),
    ('Simba','Elizabeth','cat','m','2012-04-03',NULL),
    ('Blossom','Hearts','unicorn','f','2010-11-30',NULL);


GRANT ALL PRIVILEGES ON * . * TO 'johnquser'@'localhost';