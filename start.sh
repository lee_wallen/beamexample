#!/bin/bash

docker-compose up -d
docker exec -it beamexample01_solr_1 bin/solr create_core -c 'beamtest'

# docker exec -it beamexample01_solr_1 bash -c 'solr create_collection -c beamtest -p 8983'

# docker exec -it --user=solr my_solr bin/solr create_core -c gettingstarted