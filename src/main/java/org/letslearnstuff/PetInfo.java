package org.letslearnstuff;

import org.apache.beam.sdk.coders.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.List;

@DefaultCoder(ListCoder.class)
public class PetInfo {
//
//    CREATE TABLE pet (
//            name VARCHAR(20),
//    owner VARCHAR(20),
//    species VARCHAR(20),
//    sex CHAR(1),
//    birth DATE,
//    death DATE);

    private String name;
    private String species;
    private String owner;
    private String sex;
    private String birth;
    private String death;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSpecies() {
        return species;
    }

    public void setSpecies(String species) {
        this.species = species;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getBirth() {
        return birth;
    }

    public void setBirth(String birth) {
        this.birth = birth;
    }

    public String getDeath() {
        return death;
    }

    public void setDeath(String death) {
        this.death = death;
    }

    public HashMap<String, String> getHashMap() {
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("sex", getSex());
        map.put("name", getName());
        map.put("owner", getOwner());
        map.put("species", getSpecies());
        map.put("birth", getBirth());
        map.put("death", getDeath());
        return map;
    }
}
