package org.letslearnstuff;

import org.apache.beam.runners.direct.DirectRunner;
import org.apache.beam.sdk.Pipeline;
import org.apache.beam.sdk.coders.MapCoder;
import org.apache.beam.sdk.coders.StringUtf8Coder;
import org.apache.beam.sdk.io.jdbc.JdbcIO;
import org.apache.beam.sdk.io.redis.RedisConnectionConfiguration;
import org.apache.beam.sdk.io.redis.RedisIO;
import org.apache.beam.sdk.io.solr.*;
import org.apache.beam.sdk.io.solr.SolrIO;
import org.apache.beam.sdk.options.PipelineOptions;
import org.apache.beam.sdk.options.PipelineOptionsFactory;
import org.apache.beam.sdk.transforms.MapElements;
import org.apache.beam.sdk.transforms.SimpleFunction;
import org.apache.beam.sdk.values.KV;
import org.apache.beam.sdk.values.PDone;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.request.UpdateRequest;
import org.apache.solr.common.SolrInputDocument;

import java.io.IOException;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;

/**
 * Hello world!
 *
 */
public class BeamExample01
{
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );
        BeamExample01 be = new BeamExample01();

        try {
            Pipeline p = be.setupPipeline(args);


            RedisConnectionConfiguration rcc = RedisConnectionConfiguration.create().withPort(9379);
            SolrIO.ConnectionConfiguration conn = SolrIO.ConnectionConfiguration.create("127.0.0.1:9181");
            JdbcIO.DataSourceConfiguration dsc = JdbcIO.DataSourceConfiguration.create(
                    "com.mysql.jdbc.Driver", "jdbc:mysql://localhost:9306/example01")
                    .withUsername("root");

//            SolrIO.Write write =
//                    SolrIO.write().withConnectionConfiguration(conn).to("beamtest");

            PDone lines =
                p.apply(JdbcIO.<Map<String, String>>read()
                    .withCoder((MapCoder.of(StringUtf8Coder.of(), StringUtf8Coder.of())))
                    .withDataSourceConfiguration(dsc)
                    .withQuery("select * from pet")
                    .withRowMapper(new JdbcIO.RowMapper<Map<String, String>>() {
                        public Map<String, String> mapRow(ResultSet resultSet) throws Exception {
                            HashMap<String, String> map = new HashMap<String, String>();
                            map.put("name", resultSet.getString("name"));
                            map.put("owner", resultSet.getString("owner"));
                            map.put("species", resultSet.getString("species"));
                            map.put("sex", resultSet.getString("sex"));
                            map.put("birth", resultSet.getString("birth"));
                            map.put("death", resultSet.getString("death") == null ? "" : resultSet.getString("death"));
                            return map;
                        }
                    }))

                    // write to text
                    //.apply(MapElements.via(new FormatAsTestFn()))
                    //.apply("WriteCounts", TextIO.write().to("toisting.txt"))

                    // write to redis
                    .apply(MapElements.via(new FormatAsTextFn()))
                    .apply(RedisIO.write().withConnectionConfiguration(rcc).withEndpoint("localhost", 9379));

                    // write to solr
//                    .apply(MapElements.via(new FormatAsSolrDoc()))
//                    .apply(SolrIO.write().to("beamtest"));
            //.apply(SolrIO.write().to("beamtest").withConnectionConfiguration(SolrIO.ConnectionConfiguration.create("127.0.0.1:9181")));

//            p.run().waitUntilFinish();
            p.run();

        } catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println("Done!");
        System.exit(0);
    }



    public Pipeline setupPipeline(String[] args){
        // Start by defining the options for the pipeline.
        PipelineOptions options = PipelineOptionsFactory.create();
        options.setRunner(DirectRunner.class);
        // Then create the pipeline.
        return Pipeline.create(options);
    }

    public static class FormatAsSolrDoc extends SimpleFunction<Map<String, String>, SolrInputDocument> {
        @Override
        public SolrInputDocument apply(Map<String, String> input) {
            SolrInputDocument doc = new SolrInputDocument();
            for (String key : input.keySet()) {
                doc.setField(key, input.get(key));
                if (key.equals("name"))
                    doc.setField("id", input.get(key));
            }
            System.out.println(doc.toString());
            return doc;
        }
    }

    public static class FormatAsTextFn extends SimpleFunction<Map<String, String>, KV<String, String>> {
        @Override
        public KV<String, String> apply(Map<String, String> input) {
            return KV.of(input.get("name"), input.toString());
        }
    }
}
